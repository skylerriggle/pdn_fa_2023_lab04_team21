#define DEBUG 0

#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>

#include <mpi.h>
#include <omp.h>

#include "helper.h"
#include "pagerank.h"
#include "sparse.h"

#ifndef COMPUTE_NAME
#define COMPUTE_NAME var_omp
#endif

#ifndef DISTRIBUTE_DATA_NAME
#define DISTRIBUTE_DATA_NAME var_omp_distribute
#endif

#ifndef COLLECT_DATA_NAME
#define COLLECT_DATA_NAME var_omp_collect
#endif  

#ifndef DISTRIBUTED_ALLOCATE_NAME
#define DISTRIBUTED_ALLOCATE_NAME var_omp_allocate
#endif

#ifndef DISTRIBUTED_FREE_NAME
#define DISTRIBUTED_FREE_NAME var_omp_free
#endif

static void matvec(
  multiformat_graph_t *multiformat_graph_distributed,
 	pagerank_data_t *pagerank_data_distributed
) {
  #pragma omp parallel for
  for(int cur_pos = 0; cur_pos < multiformat_graph_distributed->graph_view_coo->nnz; ++cur_pos)
  {
    int i   = multiformat_graph_distributed->graph_view_coo->row_idx[cur_pos];
    int j   = multiformat_graph_distributed->graph_view_coo->col_idx[cur_pos];
    float  val = multiformat_graph_distributed->graph_view_coo->values[cur_pos];

    pagerank_data_distributed->y[i] += val * pagerank_data_distributed->x[j];
  }
  
}

static void page_rank(
  multiformat_graph_t *multiformat_graph_distributed,
	pagerank_data_t *pagerank_data_distributed
) {
  for(int t = 0; t < pagerank_data_distributed->num_iterations; ++t)
  {
    pagerank_data_distributed->x = &(pagerank_data_distributed->buff[((t+0)%2)*multiformat_graph_distributed->m]); 
    pagerank_data_distributed->y = &(pagerank_data_distributed->buff[((t+1)%2)*multiformat_graph_distributed->m]); 
      
    for(int i = 0; i < multiformat_graph_distributed->m; ++i)
    {
	    pagerank_data_distributed->y[i] = 0.0f;
    }

    matvec(multiformat_graph_distributed, pagerank_data_distributed);

    #if DEBUG
    float res = max_pair_wise_diff_vect(multiformat_graph_distributed->m, pagerank_data_distributed->x, pagerank_data_distributed->y);
    
    printf("diff[%i]: %f\n",t, res);
    #endif
  }
}

static coo_matrix_t* create_coo_matrix_from_coo_matrix(coo_matrix_t *src_coo)
{
  coo_matrix_t *dst_coo = (coo_matrix_t *)malloc(sizeof(coo_matrix_t));

  dst_coo->m   = src_coo->m;
  dst_coo->n   = src_coo->n;
  dst_coo->nnz = src_coo->nnz;

  dst_coo->row_idx = (int *)malloc(sizeof(int)*src_coo->nnz);
  dst_coo->col_idx = (int *)malloc(sizeof(int)*src_coo->nnz);
  dst_coo->values  = (float *)malloc(sizeof(float)*src_coo->nnz);

  memcpy(dst_coo->row_idx, src_coo->row_idx, sizeof(int)*src_coo->nnz);
  memcpy(dst_coo->col_idx, src_coo->col_idx, sizeof(int)*src_coo->nnz);
  memcpy(dst_coo->values, src_coo->values, sizeof(float)*src_coo->nnz);

  return dst_coo;
}

/**********************/
/*                    */
/* START OF MPI MAGIC */
/*                    */
/**********************/

void COMPUTE_NAME(
  int num_vertices,
  int num_iterations,
	multiformat_graph_t *multiformat_graph_distributed,
	pagerank_data_t *pagerank_data_distributed
) {
  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status  status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  if(rid != root_rid) { return; }
  page_rank(multiformat_graph_distributed, pagerank_data_distributed);
}

void DISTRIBUTED_ALLOCATE_NAME(
  int num_vertices, 
  int num_iterations,
	multiformat_graph_t **multiformat_graph_distributed,
	pagerank_data_t **pagerank_data_distributed
) {
  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status  status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  /* ALLOCATE MEMORY */
  if (rid != root_rid) { return; }

  *multiformat_graph_distributed = (multiformat_graph_t *)malloc(sizeof(multiformat_graph_t));
  *pagerank_data_distributed = (pagerank_data_t *)malloc(sizeof(pagerank_data_t));
  (*pagerank_data_distributed)->buff = (float *)calloc(sizeof(float), num_vertices*2);
}

void DISTRIBUTE_DATA_NAME(
  int num_vertices,
  int num_iterations,
	multiformat_graph_t *multiformat_graph_sequential,
	pagerank_data_t *pagerank_data_sequential,
	multiformat_graph_t *multiformat_graph_distributed,
	pagerank_data_t *pagerank_data_distributed
) {

  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status  status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  if (rid != root_rid) { return; }
  
  int buff_size = sizeof(float)*2*pagerank_data_sequential->num_vertices;
  memcpy(pagerank_data_distributed->buff, pagerank_data_sequential->buff, buff_size);
           
  pagerank_data_distributed->x = &(pagerank_data_distributed->buff[0]); 
  pagerank_data_distributed->y = &(pagerank_data_distributed->buff[multiformat_graph_sequential->m]);
  pagerank_data_distributed->num_iterations = pagerank_data_sequential->num_iterations;
  pagerank_data_distributed->num_vertices = pagerank_data_sequential->num_vertices;

  multiformat_graph_distributed->m   = multiformat_graph_sequential->m;
  multiformat_graph_distributed->n   = multiformat_graph_sequential->n;
  multiformat_graph_distributed->nnz = multiformat_graph_sequential->nnz;

  multiformat_graph_distributed->degree = (long *)malloc(sizeof(long)*multiformat_graph_sequential->m);
      
  memcpy(multiformat_graph_distributed->degree, multiformat_graph_sequential->degree, sizeof(long)*multiformat_graph_sequential->m );

  multiformat_graph_distributed->graph_view_coo = create_coo_matrix_from_coo_matrix(multiformat_graph_sequential->graph_view_coo);

  multiformat_graph_distributed->graph_view_csr  = NULL;
  multiformat_graph_distributed->graph_view_csc  = NULL;
  multiformat_graph_distributed->graph_view_bcsr = NULL; 
}

void COLLECT_DATA_NAME(
  int num_vertices,
  int num_iterations,
	pagerank_data_t *pagerank_data_distributed,
	pagerank_data_t *pagerank_data_sequential
) {
  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status  status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  if(rid != root_rid) { return; }

  copy_pagerank_data(pagerank_data_sequential, pagerank_data_distributed);
  
}

void DISTRIBUTED_FREE_NAME(
  int num_vertices, 
  int num_iterations,
	multiformat_graph_t *multiformat_graph_distributed,
	pagerank_data_t *pagerank_data_distributed
) {
  int rid;
  int num_ranks;
  int tag = 0;
  MPI_Status  status;
  int root_rid = 0;

  MPI_Comm_rank(MPI_COMM_WORLD, &rid);
  MPI_Comm_size(MPI_COMM_WORLD, &num_ranks);

  /* FREE ALL MEMORY */
  if (rid != root_rid) { return; }

  free(pagerank_data_distributed->buff );
  free(pagerank_data_distributed);
  destroy_coo_matrix(multiformat_graph_distributed->graph_view_coo);
  free(multiformat_graph_distributed->degree);
  free(multiformat_graph_distributed);
}