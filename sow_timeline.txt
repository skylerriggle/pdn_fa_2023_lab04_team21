The work will be divided as follows:

Thi - Sparse File Format Section

Skyler - Parallelism in Isolation Method 1 (SIMD)

Jayendra - Parallelism in Isolation Method 2 (OpenMP)

Skyler & Jayendra - Combine the Previous Two Methods into One Combined Method


At time of writing this, we have 4 lab days (including today). From now until next Tuesday, our goal will be to iron out all of our individual portions. We will then combine everything and submit our writeup next Thursday. 