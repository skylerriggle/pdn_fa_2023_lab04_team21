#!/usr/bin/env python

import matplotlib
matplotlib.use('Agg')

import matplotlib.pyplot as plt
import pandas as pd

DATA_PATH = "../data/"

TITLE = "SIMD + OMP Variant"
PLOT_DATA = [
    ("Baseline.csv", "Baseline"),
    ("SIMD.csv", "SIMD"),
    ("OMP.csv", "OMP"),
    ("Combined.csv", "Combined"),
]

fig, ax = plt.subplots()

for data in PLOT_DATA:
    df = pd.read_csv(DATA_PATH + data[0])
    ax.plot(df['m0'], df['result'], label=data[1])
    
ax.set(xlabel="Size (p=m0)", ylabel="Performance (GFLOP/s)", title=TITLE)
plt.legend()
fig.savefig(TITLE + ".png")